# README #
### TASKS ####

* Cand se ajunge la carte sa se faca o verificare a drumuilui spre start - propun prin Dijkstra
* Unit teste pe board, verificari pentru conectare corecta si gresita
* Implementare fereastra sfml pentru citirea jucatorilor (momentan se face in cmd)
* Videoclip de prezentare

### Membrii echipei ###

* Rîtan Mihai-Lucian
* Șerban Andreea Nicoleta 
* Tutoveanu Andrei Cătălin
### Reguli joc ###

https://www.youtube.com/watch?v=FNTesrTLh4I

### Reguli privind clean code-ul ###

* Documentatie: https://github.com/wongjiahau/Clean-Code/blob/master/README.md
### Documente/ Sugestii ###
* Pentru documente suplimentare si sugestii am deschis urmatorul drive:

https://drive.google.com/open?id=1AVPGX-HIFnNJZqWdvbYe9WbdvdgNrpTr