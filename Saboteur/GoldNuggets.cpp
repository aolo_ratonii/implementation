#include "GoldNuggets.h"
void GoldNuggets::GenerateCards()
{
	//16 cards with 1 gold nuggets
	for (int i = 0; i < 16; i++)
	{
		m_unusedGold.push_back(NuggetCard(NuggetCard::Quantity(NuggetCard::Quantity::One)));
	}
	//8 cards with 2 gold nuggets
	for (int i = 0; i < 8; i++)
	{
		m_unusedGold.push_back(NuggetCard(NuggetCard::Quantity(NuggetCard::Quantity::Two)));
	}
	//4 cards with 3 gold nuggets
	for (int i = 0; i < 4; i++)
	{
		m_unusedGold.push_back(NuggetCard(NuggetCard::Quantity(NuggetCard::Quantity::Three)));
	}
}

void GoldNuggets::ShuffleCards()
{
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(m_unusedGold.begin(), m_unusedGold.end(), g);
}

void GoldNuggets::sortDescCards()
{
	std::sort(m_unusedGold.begin(), m_unusedGold.end(), DescSort);
}

bool GoldNuggets::DescSort(NuggetCard &i, NuggetCard &j)
{
	if (i.getQuantity() > j.getQuantity())
		return true;
	return false;
}

GoldNuggets::GoldNuggets()
{
	this->GenerateCards();
}

NuggetCard GoldNuggets::takeCard()
{
	NuggetCard card = m_unusedGold.back();
	m_unusedGold.pop_back();
	return card;
}

void GoldNuggets::takeCardForSaboteurs()
{
	int indexMin = 0;
	int indexMax = m_unusedGold.size() - 1;
	NuggetCard card1, card2;
	sortDescCards();

	card1 = m_unusedGold.at(indexMin);
	//indexMax--;
	if (card1.getQuantity() == NuggetCard::Quantity::Three)
	{
		card2 = m_unusedGold.at(indexMax);
		m_unusedGold.erase(m_unusedGold.begin() + indexMax);
		m_unusedGold.erase(m_unusedGold.begin() + indexMin);
	}
	else if (card1.getQuantity() == NuggetCard::Quantity::Two)
	{
		if (m_unusedGold.at(indexMin + 1).getQuantity() == NuggetCard::Quantity::Two)
			m_unusedGold.erase(m_unusedGold.begin() + indexMin + 1);
		else
			m_unusedGold.erase(m_unusedGold.begin() + indexMax - 1, m_unusedGold.begin() + indexMax + 1);
		m_unusedGold.erase(m_unusedGold.begin() + indexMin);
	}
	else
	{
		m_unusedGold.erase(m_unusedGold.begin() + indexMin, m_unusedGold.begin() + indexMin + 4);
	}
	
	ShuffleCards();
}