#include "stdafx.h"
#include "CppUnitTest.h"

#include "../ActionCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTest
{
	TEST_CLASS(ActionCardTest)
	{
	public:

		TEST_METHOD(constructorWithNoParameters)
		{
			ActionCard card;
			Assert::IsTrue(card.getLamp() == ActionCard::Lamp::None);
			Assert::IsTrue(card.getCart() == ActionCard::Cart::None);
			Assert::IsTrue(card.getPickAxe() == ActionCard::Pickaxe::None);
			Assert::IsTrue(card.getRockFall() == ActionCard::RockFall::None);
			Assert::IsTrue(card.getCardRevealed() == ActionCard::CardRevealed::None);
		}

		TEST_METHOD(constructorWithParameters)
		{
			ActionCard card(ActionCard::Lamp::Broken, ActionCard::Cart::Broken);
			Assert::IsTrue(card.getLamp() == ActionCard::Lamp::Broken);
			Assert::IsTrue(card.getCart() == ActionCard::Cart::Broken);
			Assert::IsTrue(card.getPickAxe() == ActionCard::Pickaxe::None);
			Assert::IsTrue(card.getRockFall() == ActionCard::RockFall::None);
			Assert::IsTrue(card.getCardRevealed() == ActionCard::CardRevealed::None);
		}

		TEST_METHOD(getIDTest)
		{
			ActionCard card(ActionCard::Lamp::Broken, ActionCard::Cart::Broken);
			std::tuple<int, int, int, int, int> testTuple = std::make_tuple(1, 1, 0, 0, 0);
			Assert::IsTrue(testTuple == card.getID());
		}

	};
}