#include "stdafx.h"
#include "CppUnitTest.h"

#include "../Card.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTest
{
	TEST_CLASS(CardTest)
	{
	public:

		TEST_METHOD(constructorWithType)
		{
			Card card(Card::CardType::Gold);
			Assert::IsTrue(card.getType() == Card::CardType::Gold);
		}

		TEST_METHOD(setCardsType)
		{
			Card card;
			card.setType(Card::CardType::Start);
			Assert::IsTrue(card.getType() == Card::CardType::Start);
		}
	};
}