#include "stdafx.h"
#include "CppUnitTest.h"

#include "../PathCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTest
{		
	TEST_CLASS(PathCardTest)
	{
	public:
		
		TEST_METHOD(DefaultConstructor)
		{
			PathCard::North north = PathCard::North::False;
			PathCard pathCard(north, PathCard::South::False, PathCard::West::False, PathCard::East::False, PathCard::Connected::True,Card::CardType::Path);
			Assert::IsTrue(PathCard::North::False == pathCard.getNorthValue());
			Assert::IsTrue(PathCard::South::False == pathCard.getSouthValue());
			Assert::IsTrue(PathCard::West::False == pathCard.getWestValue());
			Assert::IsTrue(PathCard::East::False == pathCard.getEastValue());
			Assert::IsTrue(pathCard.getConnectedValue() == PathCard::Connected::True);
			Assert::IsTrue(PathCard::CardType::Path == pathCard.getType());

		}

		TEST_METHOD(GetCoordTest)
		{
			PathCard pathCard(PathCard::North::True, PathCard::South::False, PathCard::West::False, PathCard::East::False, PathCard::Connected::True, Card::CardType::Path);
			std::tuple<int, int, int, int, int> testTuple = std::make_tuple(1, 0, 0, 0, 1);
			Assert::IsTrue(testTuple == pathCard.getID());
		}

	};
}