#include "stdafx.h"
#include "CppUnitTest.h"

#include "../Board.h"
#include "../PathCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTest
{
	TEST_CLASS(BoardTest)
	{
	public:

		TEST_METHOD(placeCardTest)
		{
			Board board;

			unsigned int pos = 1;
			PathCard cardToPlace(PathCard::North::True, PathCard::South::False, PathCard::West::False, PathCard::East::False, PathCard::Connected::True, Card::CardType::Path);
			
			board.setCardOnBoard(cardToPlace, pos);
			Assert::IsTrue(board.getCardFromBoard(pos).getID() == cardToPlace.getID());
		}

		TEST_METHOD(removeCardsTest)
		{
			Board board;
			
			PathCard cardToPlace(PathCard::North::True, PathCard::South::False, PathCard::West::False, PathCard::East::False, PathCard::Connected::True, Card::CardType::Path), card;
			unsigned int position = 3;
			card.setType(Card::CardType::None);
			board.setCardOnBoard(cardToPlace, position);
			Assert::IsTrue(board.getCardFromBoard(position).getID() == cardToPlace.getID());

			board.getCardFromBoardAndRemove(position);
			Assert::IsTrue(board.getCardFromBoard(position).getType() == card.getType());
		}

		TEST_METHOD(isBetweenFinishTest)
		{
			Board board;
			unsigned int position = 3;
			Assert::IsTrue(board.isBetweenFinishCard(position) == false);
			position = 9;
			Assert::IsTrue(board.isBetweenFinishCard(position) == true);
		}

		TEST_METHOD(isPathTest)
		{
			Board board;
			Assert::IsTrue(board.getIsCompletePath() == false);
		}

	};
}