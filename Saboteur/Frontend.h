#pragma once
#include <utility>
#include <stdexcept>

///* Graphic libraies
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics.hpp>

#include "Board.h"
#include "Player.h"

/* If we treat the vector board like a vector, boardSize = 45 */
static const size_t boardSize = 45;

/* Maximum number of cards in hand */
static const size_t maximumInHandCards = 6;

/* If we treat the vector board like a matrix */

/* boardRows = 5 */
static const size_t boardRows = 5;

/* boardColumns = 9 */
static const size_t boardColumns = 9;


class Frontend
{
public:

	/*A enum class that it helps us move from one state to another  */
	enum class GameState {
		Normal,
		PlaceOrDiscard,
		CardRevealed,
		RotateCard,
		SelectPlayer,
		NextPlayer,
		EndRound
	};

	//TODO if I get more time, split this class in Board, Card, TextureManager (maybe) 
public:

	Frontend();
	/*
	The actual state of the game.
	The states can be:
		Normal,
		EndRound,
		SelectPlayer,
		PlaceOrDiscard,
		NextPlayer,
		RotateCard
	*/
	GameState m_gameState;

	void resetTriggered();
	bool isKeyTrigger(sf::Keyboard::Key key);

	/*The desired FPS. (The number of updates each second).*/
	static const int FPS60 = 60;

	/* array containing all possible keys */
	bool key_triggered[sf::Keyboard::KeyCount];

	// ===================================== Textures Manager ===================================== //
public:

	sf::IntRect PathCardTextureSelecter(std::tuple<int, int, int, int, int> coord);

public:
	sf::Font m_fontTitle;
	sf::Texture m_textures;
	sf::Texture m_textureBackground;
	sf::Sprite m_background;

	// Every id card it's mapped to a texture from: Resources/path_action.png
	std::map <std::tuple<int, int, int, int, int>, sf::IntRect> m_texturePath;
	
	// Every id card it's mapped to a texture from: Resources/path_action.png
	std::map <std::tuple<int, int, int, int, int>, sf::IntRect> m_textureAction;

	std::map <int, sf::IntRect> m_textureIcons;

private:

	/* Path - Action single card width = 245*/
	static const int m_PAWidth = 245;
	/* Path - Action single card height = 350*/
	static const int m_PAHeight = 350;


	// ===================================== Objects Manager ===================================== //
public:

	///* Getters */

	/* Get in hand cards */
	std::vector<sf::RectangleShape> getInHandCards() const;

	/* Get on board cards */
	std::vector<sf::RectangleShape> getonBoardCards() const;

	/* Get players */
	std::vector<sf::Text> getPlayers() const;

	/* Get a certain card from hand - parameter1 : card postion*/
	sf::RectangleShape & getInHandCard(int);

	/* Get a certain card from board - parameter1 : card postion*/
	sf::RectangleShape & getBoardCard(int);

	/* Get a certain player - parameter1 : player postion*/
	sf::Text & getPlayer(int);

	/* Draw board on a given window */
	void drawBoard(sf::RenderWindow&, Board&, const int position = -1);

	/* Draw in hand cards */
	void drawInCurrentPlayer(sf::RenderWindow&, Player&, const int&);

	/*Draw in right of board the players name and their tools*/
	void drawThePlayers(sf::RenderWindow&, std::vector<Player> &);

	/*Simple, display the background img. I use a sf::sprite*/
	void drawBackground(sf::RenderWindow&);

	void drawFinnishWindow(sf::RenderWindow&, const std::string &);

	/* Reneder card width - deafult value = 160 */
	int renederCardWidth = 80;

	/* Reneder card height - deafult value = 230 */
	int renederCardHeight = 160;

	std::vector<sf::RectangleShape> m_cardsOnBoard;
	std::vector<sf::RectangleShape> m_cardsInHand;
	std::vector<sf::Text> m_players;
};
