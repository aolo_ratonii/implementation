#include "UnusedCards.h"
UnusedCards::UnusedCards()
{
	this->GenerateCards();
}

std::vector<std::variant<PathCard, ActionCard>> UnusedCards::getUnusedCards()
{
	return m_unusedCards;
}

void UnusedCards::GenerateCards()
{
	//TODO a function that generate automatically all the possible cards

	//Path cards
	//PathCard(North north, South south, West west, East east, Conncected connected); 

	//4 cards as this in the Stack
	for (int i = 0; i < 4; ++i)
		this->m_unusedCards.push_back(PathCard(N::True, S::True, W::True, E::True, C::True));

	//2 cards as this in the Stack
	for (int i = 0; i < 2; ++i)
		this->m_unusedCards.push_back(PathCard(N::True, S::True, W::True, E::True, C::False));

	//5 cards as this in the Stack
	for (int i = 0; i < 5; ++i)
		this->m_unusedCards.push_back(PathCard(N::False, S::True, W::False, E::True, C::True));

	//1 card in the stack as this
	this->m_unusedCards.push_back(PathCard(N::False, S::True, W::False, E::True, C::False));

	//5 cards as this in the stack
	for (int i = 0; i < 5; ++i)
		this->m_unusedCards.push_back(PathCard(N::False, S::True, W::True, E::False, C::True));

	//1 card in the stack as this
	this->m_unusedCards.push_back(PathCard(N::False, S::True, W::True, E::False, C::False));

	//5 cards as this in the stack
	for (int i = 0; i < 5; ++i)
		this->m_unusedCards.push_back(PathCard(N::True, S::True, W::False, E::False, C::False));

	//1 card in the stack as this
	this->m_unusedCards.push_back(PathCard(N::True, S::True, W::False, E::False, C::True));

	//2 cards as this in the stack
	for (int i = 0; i < 2; ++i)
		this->m_unusedCards.push_back(PathCard(N::True, S::True, W::True, E::False, C::True));

	//1 card in the stack as this
	this->m_unusedCards.push_back(PathCard(N::True, S::True, W::False, E::True, C::False));

	//5 cards as this in the stack
	for (int i = 0; i < 5; ++i)
		this->m_unusedCards.push_back(PathCard(N::False, S::False, W::True, E::True, C::True));

	//5 cards as this in the stack
	for (int i = 0; i < 5; ++i)
		this->m_unusedCards.push_back(PathCard(N::False, S::True, W::True, E::True, C::True));

	//1 card in the stack as this
	this->m_unusedCards.push_back(PathCard(N::False, S::False, W::False, E::True, C::False));

	//1 card in the stack as this
	this->m_unusedCards.push_back(PathCard(N::True, S::False, W::False, E::False, C::False));

	//Action cards
	//ActionCard(Lamp lamp = Lamp::None, Cart cart = Cart::None, PickAxe pickAxe = PickAxe::None, RockFall rockFall = RockFall::None, CardRevealed cardRevevaled = CardRevealed::None);
	
	for (int i = 0; i < 3; ++i)
	this->m_unusedCards.push_back(ActionCard(L::Broken));
	
	for (int i = 0; i < 3; ++i)
	this->m_unusedCards.push_back(ActionCard(L::None, CA::Broken));
	
	for (int i = 0; i < 3; ++i)
	this->m_unusedCards.push_back(ActionCard(L::None, CA::None, P::Broken));
	
	for (int i = 0; i < 2; ++i)
	this->m_unusedCards.push_back(ActionCard(L::Fixed));
	
	for (int i = 0; i < 2; ++i)
	this->m_unusedCards.push_back(ActionCard(L::None, CA::Fixed));
	
	for (int i = 0; i < 2; ++i)
	this->m_unusedCards.push_back(ActionCard(L::None, CA::None, P::Fixed));
	
	this->m_unusedCards.push_back(ActionCard(L::Fixed, CA::Fixed));
	
	this->m_unusedCards.push_back(ActionCard(L::Fixed, CA::None, P::Fixed));
	
	for (int i = 0; i < 3; ++i)
	this->m_unusedCards.push_back(ActionCard(L::None, CA::None, P::None, R::True));
	
	for (int i = 0; i < 6; ++i)
	this->m_unusedCards.push_back(ActionCard(L::None, CA::None, P::None, R::None, CR::True));
}

void UnusedCards::ShuffleCards()
{
	//https://en.cppreference.com/w/cpp/algorithm/random_shuffle
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(m_unusedCards.begin(), m_unusedCards.end(), g);
}

std::variant<PathCard, ActionCard>& UnusedCards::TakeCard()
{
	std::variant<PathCard, ActionCard> card = m_unusedCards.back();
	m_unusedCards.pop_back();
	return card;
}