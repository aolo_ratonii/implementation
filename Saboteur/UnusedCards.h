#pragma once
#include "PathCard.h"
#include "ActionCard.h"

#include <variant>
#include <vector>
#include <random>

///* Shortcuts for path cards
using N = PathCard::North;
using S = PathCard::South;
using W = PathCard::West;
using E = PathCard::East;
using C = PathCard::Connected;

///* Shortcut for action cards
using L = ActionCard::Lamp;
using P = ActionCard::Pickaxe;
using CA = ActionCard::Cart;
using CR = ActionCard::CardRevealed;
using R = ActionCard::RockFall;


class UnusedCards
{

public:
	static std::variant<PathCard, ActionCard> kInvalid;
	
	/*Default constructor*/
	UnusedCards();
	
	///*Getters
	std::vector<std::variant<PathCard, ActionCard>> getUnusedCards();
	void GenerateCards();
	
	void ShuffleCards();
	std::variant<PathCard, ActionCard>& TakeCard();

private:
	/* The deck of unused cards */
	std::vector<std::variant<PathCard, ActionCard>> m_unusedCards;
};

