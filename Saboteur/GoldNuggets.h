#pragma once
#include"NuggetCard.h"

#include <variant>
#include <random>
#include <vector>
#include <algorithm>


class GoldNuggets
{
public:
	/*Default constructor*/
	GoldNuggets();
	
	void GenerateCards();
	void ShuffleCards();
	void sortDescCards();
	static bool DescSort(NuggetCard &i,NuggetCard &j);
	NuggetCard takeCard();
	void takeCardForSaboteurs();

private:
	std::vector<NuggetCard> m_unusedGold;
};

