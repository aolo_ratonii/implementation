#include<iostream>
#include<thread>
#include<vector>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib

SOCKET Connections[10];
unsigned int nrConnections = 0;
SOCKET ListenSocket = INVALID_SOCKET;
int iResult;

//static std::vector <std::thread> threads;
std::vector <std::string> names;
std::vector <int> ages;

#define DEFAULT_BUFLEN 512
#define NR_PLAYERS 3

int ReadClientInfo(const SOCKET& ClientSocket, int index) 
{
	// *** Recieve and send data ***
	char recvbuf[DEFAULT_BUFLEN];
	char sendbuf[DEFAULT_BUFLEN];
	int iRecvResult, iSendResult;
	int recvbuflen = DEFAULT_BUFLEN;

	//get name
	sprintf_s(sendbuf, "Welcome! Introduce your name please:\n");
	iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
	if (iSendResult == SOCKET_ERROR)
	{
		printf("send failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	iRecvResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
	if (iRecvResult > 0)
	{
		printf("Client%d: %.*s\n", index, iRecvResult, recvbuf);
		recvbuf[iRecvResult] = 0;
		//printf("%s\n", recvbuf);
		std::string name(recvbuf);
		names[index] = name;
		//std::cout << names[0].c_str() << std::endl;
		sprintf_s(sendbuf, "Thank you, %s!\n", recvbuf);
		iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
		if (iSendResult == SOCKET_ERROR)
		{
			printf("send failed: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}
	}
	else if (iRecvResult == 0)
		printf("Connection closing...\n");
	else
	{
		printf("recv failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	//get age:
	sprintf_s(sendbuf, "Introduce your age, please: ");
	iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
	if (iSendResult == SOCKET_ERROR)
	{
		printf("send failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	iRecvResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
	if (iRecvResult > 0)
	{
		printf("Client%d: %.*s\n", index, iRecvResult, recvbuf);
		recvbuf[iRecvResult] = 0;
		int age = 0;
		for (int index = 0; recvbuf[index] >= '0' && recvbuf[index] <= '9'; index++)
		{
			age = age * 10 + recvbuf[index] - '0';
		}
		//std::cout << age<<std::endl;
		ages[index]=age;
		sprintf_s(sendbuf, "Thank you!\n");
		iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
		if (iSendResult == SOCKET_ERROR)
		{
			printf("send failed: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}
	}
	else if (iRecvResult == 0)
		printf("Connection closing...\n");
	else
	{
		printf("recv failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

}

int ClientGetPrintFunction(const SOCKET& ClientSocket, int index)
{
	char recvbuf[DEFAULT_BUFLEN];
	char sendbuf[DEFAULT_BUFLEN];
	int iRecvResult, iSendResult;
	int recvbuflen = DEFAULT_BUFLEN;

	sprintf_s(sendbuf, "It's your turn!", recvbuf);
	iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
	if (iSendResult == SOCKET_ERROR)
	{
		printf("send failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	iRecvResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
	if (iRecvResult > 0)
	{
		//printf("Bytes received: %d\n", iRecvResult);
		printf("Client%d: %.*s\n", index, iRecvResult, recvbuf);
		// Thank the client :)
		sprintf_s(sendbuf, "Thank you!", recvbuf);
		iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
		
		if (iSendResult == SOCKET_ERROR)
		{
			printf("send failed: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

		//printf("Bytes sent: %d\n", iSendResult);
	}
	else if (iRecvResult == 0)
		printf("Connection closing...\n");
	else
	{
		printf("recv failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}
}

int ClientExitMessage(const SOCKET& ClientSocket, int index)
{
	char sendbuf[DEFAULT_BUFLEN];
	int iSendResult;
	sprintf_s(sendbuf, "exit");
	iSendResult = send(ClientSocket, sendbuf, strlen(sendbuf), 0);
	if (iSendResult == SOCKET_ERROR)
	{
		printf("send failed: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}
}

int connectToServer(int &nrPlayers,std::vector<std::string>& namesMain, std::vector<int>& agesMain)
{
	names.resize(NR_PLAYERS);
	ages.resize(NR_PLAYERS);

	nrPlayers = NR_PLAYERS;

	// *** Initialize Winsock ***
	// initialize Winsock before making other Winsock functions calls
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
	if (iResult != 0)
	{
		printf("WSAStartup failed: %d\n", iResult);
		return 1;
	}

	// *** Create socket ***
#define DEFAULT_PORT "27015"
	struct addrinfo *result = NULL, *ptr = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_STREAM;	// stream
	hints.ai_protocol = IPPROTO_TCP;	// tcp
	hints.ai_flags = AI_PASSIVE;		// we intend to use the socket in a call to bind

	// Resolve the local address and port to be used by the server
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for the server to listen for client connections
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET)
	{
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// *** Binding the socket ***
	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// 'result' not needed anymore so free it
	freeaddrinfo(result);


	// *** Listening on the socket ***
	if (listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		printf("Listen failed with error: %ld\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}


	for (int i = 0; i < NR_PLAYERS; i++)
	{
		SOCKET ClientSocket = INVALID_SOCKET;
		// Accept a client socket
		ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET)
		{
			printf("accept failed: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
			return 1;
		}
		else
		{
			printf("client%d accepted\n", i);
			nrConnections++;
			Connections[i] = ClientSocket;
			ReadClientInfo(ClientSocket, i);
		}
	}

	//for (int i = 0; i < NR_PLAYERS; i++) 
	//{
	//	std::cout << names[i].c_str() << std::endl;
	//	std::cout << ages[i] << std::endl;
	//}
	namesMain = names;
	agesMain = ages;
}

int readServer()
{
	while (true)
	{
		for (int i = 0; i < nrConnections; i++)
		{
			ClientGetPrintFunction(Connections[i], i);
		}
		break;
	}
	return 0;
}

int disconnectServer()
{
	for (int i = 0; i < nrConnections; i++)
	{
		ClientExitMessage(Connections[i], i);
	}

	// cleanup
	for (int index = 0; index < nrConnections; index++)
	{
		// *** Disconnecting the Server ***
		// shutdown the send half of the connection since no more data will be sent
		iResult = shutdown(Connections[index], SD_SEND);
		if (iResult == SOCKET_ERROR) {
			printf("shutdown failed: %d\n", WSAGetLastError());
			closesocket(Connections[index]);
			WSACleanup();
			return 1;
		}
		closesocket(Connections[index]);
	}

	// No longer need server socket
	closesocket(ListenSocket);

	WSACleanup();

	return 0;
}
