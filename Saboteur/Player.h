#pragma once
#include "Card.h"
#include "GoldNuggets.h"
#include "NuggetCard.h"
#include "Board.h"
#include "UnusedCards.h"
#include "Logging/Logging.h"

#include <cstdint>
#include <string>
#include <iostream>
#include <vector>
#include <queue>


class Player
{
public:
	//A class that define what type of player a player is
	enum class PlayerType : uint8_t
	{
		Digger,
		Saboteur
	};

public:

	//Parametric constructor
	Player() = default;
	Player(const std::string& name, PlayerType playerType = PlayerType::Digger);

	//At the beginning of his turn a player has the following possibilities to start: 
		//Place a path, 
		//Put an action card,
		//Discard one of his cards

	//Setter
	void setName(std::string name);
	void setType(PlayerType type);
	void setAge(unsigned int age);
	void setLamp(bool lamp);
	void setPickAxe(bool pickAxe);
	void setCart(bool cart);
	void setCard(PathCard&, const int &);

	//Getter
	unsigned int getAge() const;
	bool getLamp();
	bool getPickAxe();
	bool getCart();
	int32_t getGold();
	std::string getName() const;
	Player::PlayerType getType() const;
	std::string getTypeAsString() const;
	PathCard getPathCard(const int& pos);

	/*
		With this function we place a care from hand to the board
		First parm  - the board where to place
		Second parm - the postion of card in hand
		Third parm  - the postion on board

		Returns a boolean - True (was placed correct) / False (was placed correct)
	*/
	bool PlacePathCard(Board &board, const int &posInHand, const int &pos);
	
	/*
		This function destory a card from board
		First parm - the board
		Second parm - the postion of the card that we want to be destoryed
	*/
	bool removeCardFromBoard(Board &, const int &);

	bool isAnyPathFromGoldToFinish(Board &board, const int &pos);
	bool isBetweenFinishCard(const int &pos);
	bool isOnTheFirstRowOfBoard(const int &pos);
	bool isOnTheLastRowOfTheBoard(const int &pos);
	bool isOnTheLastColumnOfTheBoard(const int &pos);
	
	void PlaceActionCard(std::vector<Player>& players);
	bool PlaceActionCardAtPlayer(Player& player, ActionCard& card);

	void Discard(const std::variant<PathCard, ActionCard> &card);
	
	static void RotateCardToFit(PathCard& pathCard);
	
	void ShowInHandCards();
	std::variant<PathCard, ActionCard> selectCard(int noOfCards);

	//At the end of the tour the player will take a card from the available stack of cards if there are any cards.
	void PickUnusedCards(UnusedCards& unusedCards);

	friend std::ostream& operator << (std::ostream& os, const Player& player);
	friend std::istream& operator >> (std::istream& is, Player player);

	std::vector<std::variant<PathCard, ActionCard>>& getHandsCards();
	void setHandsCards(std::vector<std::variant<PathCard, ActionCard>> arrOfCards);
	
	//Actions
	bool m_actionLamp = false;
	bool m_actionPickAxe = false;
	bool m_actionCart = false;

	int32_t amountOfGold = 0;

private:
	PlayerType m_playerType : 1;
	std::string m_name;

	//The playing cards that the player has in his hand
	std::vector<std::variant<PathCard, ActionCard>> m_inHandCards;
	std::vector<NuggetCard> m_goldCards;
	unsigned int m_age;
};