#pragma once
#include "NuggetCard.h"

NuggetCard::NuggetCard(Quantity amount)
{
	m_amount = amount;
}

void NuggetCard::setQuantity(Quantity amount)
{
	amount = m_amount;
}

NuggetCard::Quantity NuggetCard::getQuantity()
{
	return m_amount;
}
