#include "Player.h"
#include "PathCard.h"
#include <iterator>

Player::Player(const std::string & name, PlayerType playerType) :
	m_name(name),
	m_playerType(playerType)
{
	//empty
}

void Player::PickUnusedCards(UnusedCards& unusedCards)
{
	if (unusedCards.getUnusedCards().size() > 0)
	{
		std::variant<PathCard, ActionCard> card = unusedCards.TakeCard();

		if (true)
		{
			using CurrentCardType = std::decay_t<decltype(card)>;

			if (std::holds_alternative<PathCard>(card))
				m_inHandCards.push_back(std::move(card));

			else if (std::holds_alternative<ActionCard>(card))
				m_inHandCards.push_back(std::move(card));

			else
			{
				Logger log(std::cout, Logger::Level::Info);
				log.log("Unused cards holds only Path and Action cards!", Logger::Level::Warning);
			}
		}
		else
		{
			///========================== DEBUGING ==========================///
			std::cout << "\nNo more cards in deck";
			///========================== DEBUGING ==========================///
		}
	}
}

std::vector<std::variant<PathCard, ActionCard>>& Player::getHandsCards()
{
	return m_inHandCards;
}

void Player::setHandsCards(std::vector<std::variant<PathCard, ActionCard>>arrOfCards)
{
	m_inHandCards = arrOfCards;
}

void Player::setName(std::string name)
{
	m_name = name;
}

void Player::setType(PlayerType type)
{
	m_playerType = type;
}

void Player::setAge(unsigned int age)
{
	m_age = age;
}

void Player::setLamp(bool lamp)
{
	m_actionLamp = lamp;
}

void Player::setPickAxe(bool pickAxe)
{
	m_actionPickAxe = pickAxe;
}

void Player::setCart(bool cart)
{
	m_actionCart = cart;
}

void Player::setCard(PathCard & path, const int & pos)
{
	m_inHandCards.at(pos) = path;
}

unsigned int Player::getAge() const
{
	return m_age;
}

bool Player::getLamp()
{
	return m_actionLamp;
}

bool Player::getPickAxe()
{
	return m_actionPickAxe;
}

bool Player::getCart()
{
	return m_actionCart;
}

int32_t Player::getGold()
{
	return amountOfGold;
}

std::string Player::getName() const
{
	return m_name;
}

Player::PlayerType Player::getType() const
{
	return m_playerType;
}

std::string Player::getTypeAsString() const
{
	if (m_playerType == PlayerType::Digger)
		return std::string("Digger");
	else /* if (m_playerType == Player::Saboteur)*/
		return std::string("Saboteur");
}

PathCard Player::getPathCard(const int & pos)
{
	return std::get<PathCard>(m_inHandCards.at(pos));
}

bool Player::PlacePathCard(Board &board, const int &posInHand, const int &pos)
{
	if (m_inHandCards.size() == 0)
	{
		std::cout << "You don't have any card in your hand!\n";
		return false;
	}
	std::variant<PathCard, ActionCard> card = m_inHandCards.at(posInHand);
	PathCard pathCard = std::get<PathCard>(card);
	if (board.getCardFromBoard(pos).getType() == Card::CardType::None)
	{

		//board.at(pos).getType()
		bool atLeastOneNeighbourIsPathCard = false;

		bool okOnNorth = false;
		int northPos;

		bool okOnSouth = false;
		int southPos;

		bool okOnWest = false;
		int westPos;

		bool okOnEast = false;
		int eastPos;

		if (isBetweenFinishCard(pos))
		{
			std::cout << "You can't place a card between finish cards!!\n";
			return false;
		}
		//for North
		northPos = pos - board.getkLengthBoard();
		if (pathCard.getNorthValue() == PathCard::North::True)
		{
			if (isOnTheFirstRowOfBoard(pos))//First row of the board
				okOnNorth = true;
			else if (board.getCardFromBoard(northPos).getType() == Card::CardType::None)
				okOnNorth = true;
			else if (board.getCardFromBoard(northPos).getType() == Card::CardType::Start)
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnNorth = true;
			}
			else if (((int)board.getCardFromBoard(northPos).getSouthValue() == (int)pathCard.getNorthValue()))
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnNorth = true;
			}
			else
				okOnNorth = false;
		}
		else if (pathCard.getNorthValue() == PathCard::North::False)
		{
			if (northPos < 0)
				okOnNorth = true;
			else if (board.getCardFromBoard(northPos).getType() == Card::CardType::None)
				okOnNorth = true;
			else if (board.getCardFromBoard(northPos).getType() == Card::CardType::Path &&
				board.getCardFromBoard(northPos).getSouthValue() == PathCard::South::False)
			{
				okOnNorth = true;
			}
		}
		//For South
		southPos = pos + board.getkLengthBoard();
		if (pathCard.getSouthValue() == PathCard::South::True)
		{
			if (isOnTheLastRowOfTheBoard(pos))
				okOnSouth = true;
			else if (board.getCardFromBoard(southPos).getType() == Card::CardType::None)
				okOnSouth = true;
			else if (board.getCardFromBoard(southPos).getType() == Card::CardType::Start)
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnSouth = true;
			}
			else if ((int)board.getCardFromBoard(southPos).getNorthValue() == (int)pathCard.getSouthValue())
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnSouth = true;
			}
			else
				okOnSouth = false;
		}
		else if (pathCard.getSouthValue() == PathCard::South::False)
		{
			if (southPos > board.getBoardSize() - 1)
				okOnSouth = true;
			else if (board.getCardFromBoard(southPos).getType() == Card::CardType::None)
				okOnSouth = true;
			else if (board.getCardFromBoard(southPos).getType() == Card::CardType::Path &&
				board.getCardFromBoard(southPos).getNorthValue() == PathCard::North::False)
				okOnSouth = true;
		}

		//For East
		eastPos = pos + 1;
		if (pathCard.getEastValue() == PathCard::East::True)
		{
			if (isOnTheLastColumnOfTheBoard(pos))
				okOnEast = true;
			else if (board.getCardFromBoard(eastPos).getType() == Card::CardType::Start)
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnEast = true;
			}
			else if (board.getCardFromBoard(eastPos).getType() == Card::CardType::Path &&
				(int)board.getCardFromBoard(eastPos).getWestValue() == (int)pathCard.getEastValue())
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnEast = true;
			}
			else if (board.getCardFromBoard(eastPos).getType() == Card::CardType::None)
				okOnEast = true;
			else
				okOnEast = false;
		}
		else if (pathCard.getEastValue() == PathCard::East::False)
		{
			if (isOnTheLastColumnOfTheBoard(pos))
				okOnEast = true;
			if (board.getCardFromBoard(eastPos).getType() == Card::CardType::None)// pos or pos+1 ??
				okOnEast = true;
			else if (board.getCardFromBoard(eastPos).getWestValue() == PathCard::West::False)//Modified to false
			{
				okOnEast = true;
			}
			else if (board.getCardFromBoard(eastPos).getWestValue() == PathCard::West::False &&
				pathCard.getEastValue() == PathCard::East::False)
			{
				okOnEast = true;
			}
		}

		//for West
		westPos = pos - 1;
		if (pathCard.getWestValue() == PathCard::West::True)
		{
			if (pathCard.getWestValue() == PathCard::West::True &&
				board.getCardFromBoard(westPos).getType() == Card::CardType::FinishGold &&
				okOnNorth == true && okOnSouth == true && okOnEast == true && atLeastOneNeighbourIsPathCard == true)
			{
				board.setCardOnBoard(std::move(m_inHandCards.at(posInHand)), pos);
				m_inHandCards.erase(m_inHandCards.begin() + posInHand);
				if (isAnyPathFromGoldToFinish(board, pos))
				{
					board.setIfIsCompletePath(true);
					std::cout << "You've reached finish!!\n";
					return true;
				}
			}
			else if (pathCard.getWestValue() == PathCard::West::True &&
				board.getCardFromBoard(westPos).getType() == Card::CardType::Finish &&
				okOnNorth == true && okOnSouth == true && okOnEast == true && atLeastOneNeighbourIsPathCard == true)
			{
				board.setCardOnBoard(std::move(m_inHandCards.at(posInHand)), pos);
				m_inHandCards.erase(m_inHandCards.begin() + posInHand);
				std::cout << "You've reached to a rock\n";
				okOnWest = true;
				return false;
				//Should reveal rock finishCard
			}

			else if ((int)board.getCardFromBoard(westPos).getEastValue() == (int)pathCard.getWestValue())
			{
				atLeastOneNeighbourIsPathCard = true;
				okOnWest = true;
			}
			else if (board.getCardFromBoard(westPos).getType() == Card::CardType::None)
				okOnWest = true;
		}
		else if (pathCard.getWestValue() == PathCard::West::False)
		{
			if (board.getCardFromBoard(westPos).getType() == Card::CardType::None)
				okOnWest = true;
			else if (board.getCardFromBoard(westPos).getEastValue() == PathCard::East::False)//Modify from true to false
			{
				okOnWest = true;
			}
			else if (board.getCardFromBoard(westPos).getEastValue() == PathCard::East::False &&
				pathCard.getWestValue() == PathCard::West::False)
			{
				okOnWest = true;
			}
		}

		//Verify if the card is eligible to be placed on the board
		if (okOnNorth == true && okOnSouth == true && okOnWest == true && okOnEast == true && atLeastOneNeighbourIsPathCard == true)
		{
			board.setCardOnBoard(std::move(m_inHandCards.at(posInHand)), pos);
			m_inHandCards.erase(m_inHandCards.begin() + posInHand);
			return true;
		}
		else
		{
			std::cout << "Your card hasn't been placed at place number " << pos << "!!\n";
			if (!okOnNorth)
				std::cout << "There is no connection at north\n";
			if (!okOnSouth)
				std::cout << "There is no connection at south\n";
			if (!okOnEast)
				std::cout << "There is no connection at east\n";
			if (!okOnWest)
				std::cout << "There is no connection at west\n";
			return false;
		}
	}
	else
	{
		std::cout << "The place is occupied by another card\n";
		return false;
	}
	return false;
}

bool Player::removeCardFromBoard(Board &board, const int & pos)
{
	/* Destroy the card only if there is a crad to be destoryed*/
	if (board.getCardFromBoard(pos).getType() != Card::CardType::None &&
		board.getCardFromBoard(pos).getType() != Card::CardType::Finish &&
		board.getCardFromBoard(pos).getType() != Card::CardType::FinishGold &&
		board.getCardFromBoard(pos).getType() != Card::CardType::Start)
	{
		/*on the removed card the place becomes empty and initialize with None type card*/
		board.getCardFromBoard(pos).setType(Card::CardType::None);
		return true;
	}
	return false;
}

bool Player::isAnyPathFromGoldToFinish(Board & board, const int & pos)
{
	std::queue<std::tuple<Card, int>> queueCards;
	Card current;
	int lastElementFromQueue = pos - 1;
	bool elements[45] = { false };
	current = board.getCardFromBoard(pos);
	std::tuple<Card, int> mytuple(current, pos);
	queueCards.push(mytuple);
	elements[pos] = true;
	auto currentPos = pos;

	auto eastPos = 0;
	auto westPos = 0;
	auto southPos = 0;
	auto northPos = 0;
	while (queueCards.size() > 0)
	{

		//EAST
		eastPos = currentPos + 1;
		if (currentPos != 9 && currentPos != 0 && currentPos != 18 && currentPos != 27 && currentPos != 36)
		{
			if (board.getCardFromBoard(eastPos).getType() != Card::CardType::None &&
				board.getCardFromBoard(eastPos).getConnectedValue() == PathCard::Connected::True)
			{
				if (elements[eastPos] == false)
				{
					elements[eastPos] = true;
					queueCards.push(std::make_tuple(board.getCardFromBoard(eastPos), eastPos));
				}
			}
			else if (board.getCardFromBoard(eastPos).getType() == Card::CardType::Start)
				queueCards.push(std::make_tuple(board.getCardFromBoard(eastPos), eastPos));
		}

		//WEST
		auto westPos = currentPos - 1;
		if (board.getCardFromBoard(westPos).getType() != Card::CardType::None &&
			board.getCardFromBoard(westPos).getConnectedValue() == PathCard::Connected::True)
		{
			if (elements[westPos] == false)
			{
				elements[westPos] = true;
				queueCards.push(std::make_tuple(board.getCardFromBoard(westPos), westPos));
			}
			else if (board.getCardFromBoard(westPos).getType() == Card::CardType::Start)
				queueCards.push(std::make_tuple(board.getCardFromBoard(westPos), westPos));
		}

		//SOUTH
		southPos = currentPos + board.getkLengthBoard();
		if (southPos < board.getBoardSize())
		{
			if (board.getCardFromBoard(southPos).getType() != Card::CardType::None &&
				board.getCardFromBoard(southPos).getConnectedValue() == PathCard::Connected::True)
			{
				if (elements[southPos] == false)
				{
					elements[southPos] = false;
					queueCards.push(std::make_tuple(board.getCardFromBoard(southPos), southPos));
				}
			}
			else if (board.getCardFromBoard(eastPos).getType() == Card::CardType::Start)
				queueCards.push(std::make_tuple(board.getCardFromBoard(eastPos), eastPos));
		}


		//NORTH
		northPos = currentPos - board.getkLengthBoard();
		if (northPos > 0)
		{
			if (board.getCardFromBoard(northPos).getType() != Card::CardType::None &&
				board.getCardFromBoard(northPos).getConnectedValue() == PathCard::Connected::True)
			{
				if (elements[northPos] == false)
				{
					elements[northPos] = true;
					queueCards.push(std::make_tuple(board.getCardFromBoard(northPos), northPos));
				}
			}
			else if (board.getCardFromBoard(northPos).getType() == Card::CardType::Start)
				queueCards.push(std::make_tuple(board.getCardFromBoard(northPos), northPos));
		}

		queueCards.pop();
		if (queueCards.size() == 0)
		{
			return false;
		}
		current = std::get<0>(queueCards.front());
		currentPos = std::get<1>(queueCards.front());

		if (current.getType() == Card::CardType::Start)
		{
			return true;
		}

	}
	return false;
}

bool Player::isBetweenFinishCard(const int & pos)
{
	if (pos == 9 || pos == 27)
		return true;
	return false;
}

bool Player::isOnTheFirstRowOfBoard(const int & pos)
{
	if (pos < 9 && pos > 0)
		return true;
	return false;
}

bool Player::isOnTheLastRowOfTheBoard(const int & pos)
{
	if (pos < 45 && pos > 36)
		return true;
	else
		return false;
}

bool Player::isOnTheLastColumnOfTheBoard(const int & pos)
{
	if (pos == 8 || pos == 17 || pos == 35 || pos == 44)
		return true;
	else
		return false;
}

void Player::PlaceActionCard(std::vector<Player>& players)
{
	int noCard;
	std::string name;
	std::cout << "What action card do you want to use: ";
	std::cin >> noCard;
	int indexName = 1;
	std::cout << "Those are the players: \n";
	for (Player p : players)
	{
		//we suppose that players have different names
		if (p.m_name != m_name)
		{
			std::cout << indexName << "." << p.m_name << " and his attributes: \n ActionCart "
				<< (int)p.m_actionCart << " ActionLamp" << (int)p.m_actionLamp << " PickAxe" << (int)p.m_actionPickAxe << std::endl;
			indexName++;
		}
	}
	std::cout << "Write the player name that you want to use it for: ";
	std::cin >> name;
	ActionCard actionCard = std::get<ActionCard>(getHandsCards().at(noCard));
	int indexOfPlayerToAttack = 0;
	for (Player p : players)
	{
		if (p.m_name == name)
			break;
		else
			++indexOfPlayerToAttack;
	}

	//if(actionCard.getCart())
	//if(players.at(indexOfPlayerToAttack))

}

bool Player::PlaceActionCardAtPlayer(Player& player, ActionCard & card)
{
	if (card.getLamp() == L::Broken)
	{
		player.m_actionLamp = true;
		return true;
	}

	if (card.getCart() == CA::Broken)
	{
		player.m_actionCart = true;
		return true;
	}

	if (card.getPickAxe() == P::Broken)
	{
		player.m_actionPickAxe = true;
		return true;
	}
	if (card.getLamp() == L::Fixed)
	{
		player.m_actionLamp = false;
		return true;
	}
	if (card.getCart() == CA::Fixed)
	{
		player.m_actionCart = false;
		return true;
	}
	if (card.getPickAxe() == P::Fixed)
	{
		player.m_actionPickAxe = false;
		return true;
	}
	return false;
}

void Player::Discard(const std::variant<PathCard, ActionCard> &card)
{

}

void Player::RotateCardToFit(PathCard& card)
{
	if (static_cast<int>(card.getNorthValue()) != static_cast<int>(card.getSouthValue()))
	{
		PathCard::South south = (PathCard::South)card.getNorthValue();
		card.setNorthValue((PathCard::North)card.getSouthValue());
		card.setSouthValue(south);
	}
	if (static_cast<int>(card.getWestValue()) != static_cast<int>(card.getEastValue()))
	{
		PathCard::East east = (PathCard::East)card.getWestValue();
		card.setWestValue((PathCard::West)card.getEastValue());
		card.setEastValue(east);
	}
}

void Player::ShowInHandCards()
{
	auto lambdaPrintVisitor = [](auto&& card)
	{
		std::cout << card;
		using T = std::decay_t<decltype(card)>;
		if constexpr (std::is_same_v<T, PathCard>)
			std::cout << " - PathCard\n";
		else std::cout << " - ActionCard\n";
	};
	for (const auto& nextVariant : m_inHandCards)
		std::visit(lambdaPrintVisitor, nextVariant);
}

std::variant<PathCard, ActionCard> Player::selectCard(int noOfCards)
{
	if (std::holds_alternative<PathCard>(m_inHandCards.at(noOfCards)))
		return std::get<PathCard>(m_inHandCards.at(noOfCards));
	return std::get<ActionCard>(m_inHandCards.at(noOfCards));
}



std::ostream & operator<<(std::ostream & os, const Player & player)
{
	return os << player.m_name << " " << static_cast<int>(player.m_playerType);
}

std::istream& operator>>(std::istream & is, Player player)
{
	is >> player.m_name;
	return is;
}

