#include "Frontend.h"

#include<iostream>

static constexpr std::tuple startCard = std::tuple(3, 3, 3, 3, 3);

static constexpr std::tuple finishGoldCardUnhide = std::tuple(2, 2, 2, 2, 2);

static constexpr std::tuple finishCardHide = std::tuple(0, -1, -1, -1, -1);


Frontend::Frontend()
{
	///=================================== SETUP BACKEND =================================//
	/* initialize game state */
	m_gameState = GameState::Normal;

	/* inintialize the board */
	m_cardsOnBoard.resize(boardSize);

	/* initialize the hand */
	m_cardsInHand.resize(maximumInHandCards);

	///=================================== LOAD RESOURCES =================================//
	/* load path textures */
	m_textures.loadFromFile("../../Textures/textures.png");

	/* load title font */
	m_fontTitle.loadFromFile("../../Fonts/CONSOLA.TTF");

	/* load background texture */
	m_textureBackground.loadFromFile("../../Textures/background.png");


	m_background.setTexture(m_textureBackground);
	/* Set the texture postion for each path card*/

	/*
	on Path std::tuple<int, int, int, int, int> <=> std::tuple <N,S,W,E,C>
	on Action std::tuple<int, int, int, int, int> <=> std::tuple <Lamp, Cart, Pickaxe, RockFall, CardReaveal>
	*/
	std::tuple<int, int, int, int, int> card;
	sf::IntRect bounds; // Texture position

	unsigned row0 = 0;
	unsigned row1 = m_PAHeight;
	unsigned column0 = 0;
	unsigned column1 = m_PAWidth;

	//2 * row1 is the second row

	///===================================== PATH CARDS ===================================//
	/* Path Card - Start Card id (3, 3, 3, 3, 3) */
	card = startCard;
	bounds = sf::IntRect(column1, 6 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	/* Path Card - Finish Card Hide id (0, -1, -1, -1, -1) */
	card = finishCardHide;
	bounds = sf::IntRect(7 * column1, 6 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	/* Path Card - Finish Gold Card Unhide id (2, 2, 2, 2, 2) */
	card = finishGoldCardUnhide;
	bounds = sf::IntRect(2 * column1, 6 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));
	
	///=================================== (0, 1, 0, 1, 0) =================================//
	/*
												   1
											 +-----------+
											 |           |
											 |           |
											 |           |
											 |       +---+
											0|       |   |1
											 |   +-------+
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 +---+---+---+
												   1

	*/

	card = std::tuple(0, 1, 0, 1, 0);
	bounds = sf::IntRect(column0, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	///=================================== (0, 1, 1, 0, 0) =================================//
	/*
												   0
											 +-----------+
											 |           |
											 |           |
											 |           |
											 +---+       |
											1|   |       |0
											 +-------+   |
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 +---+---+---+
												   1

	*/

	card = std::tuple(0, 1, 1, 0, 0);
	bounds = sf::IntRect(column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	///=================================== (1, 1, 0, 0, 0) =================================//
	/*
												   1
											 +---+---+---+
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 |   +---+   |
											0|           |0
											 |   +---+   |
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 +---+---+---+
												   1
	*/
	card = std::tuple(1, 1, 0, 0, 0);
	bounds = sf::IntRect(2 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(0, 0, 0, 1, 0);
	bounds = sf::IntRect(3 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 0, 0, 0);
	bounds = sf::IntRect(4 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 1, 0, 1, 0);
	bounds = bounds = sf::IntRect(5 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 1, 1, 1, 0);
	bounds = sf::IntRect(6 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(0, 0, 1, 1, 0);
	bounds = sf::IntRect(7 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 1, 1, 0);
	bounds = sf::IntRect(8 * column1, 2 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	///=================================== (0, 0, 1, 1, 1) =================================//
	/*
												   0
											 +-----------+
											 |           |
											 |           |
											 |           |
											 +-----------+
											1|           |1
											 +-----------+
											 |           |
											 |           |
											 |           |
											 +-----------+
												   0
	*/

	card = std::tuple(0, 0, 1, 1, 1);
	bounds = sf::IntRect(column0, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 1, 0, 0);
	bounds = sf::IntRect(column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 0, 1, 0);
	bounds = sf::IntRect(2 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	///=================================== (0, 0, 1, 0, 0) =================================//
	/*
												   0
											 +-----------+
											 |           |
											 |           |
											 |           |
											 +-------+   |
											1|       |   |0
											 +---+   |   |
											 |   |   |   |
											 |   +---+   |
											 |           |
											 +-----------+
												   0

	*/
	card = std::tuple(0, 0, 1, 0, 0);
	bounds = sf::IntRect(3 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));
	///=================================== (0, 1, 0, 0, 0) =================================//
	/*
												   0
											 +-----------+
											 |           |
											 |           |
											 |   +---+   |
											 |   |   |   |
											0|   |   |   |0
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 +---+---+---+
												   1

	*/
	card = std::tuple(0, 1, 0, 0, 0);
	bounds = sf::IntRect(4 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(0, 1, 1, 1, 1);
	bounds = sf::IntRect(5 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 1, 1, 1);
	bounds = sf::IntRect(6 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(0, 1, 1, 1, 0);
	bounds = sf::IntRect(7 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 1, 1, 0, 0);
	bounds = sf::IntRect(8 * column1, 3 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(0, 1, 0, 1, 1);
	bounds = sf::IntRect(column0, 4 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 1, 0, 1);
	bounds = sf::IntRect(2 * column1, 4 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 0, 0, 1, 1);
	bounds = sf::IntRect(3 * column1, 4 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(0, 1, 1, 0, 1);
	bounds = sf::IntRect(5 * column1, 4 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 1, 0, 0, 1);
	bounds = sf::IntRect(column0, 5 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 1, 1, 0, 1);
	bounds = sf::IntRect(5 * column1, 5 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	card = std::tuple(1, 1, 0, 1, 1);
	bounds = sf::IntRect(6 * column1, 5 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	///=================================== (1, 1, 1, 1, 1) =================================//
	/*
												   1
											 +---+---+---+
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 +---+   +---+
											1|           |1
											 +---+   +---+
											 |   |   |   |
											 |   |   |   |
											 |   |   |   |
											 +---+---+---+
												   1
	*/

	card = std::tuple(1, 1, 1, 1, 1);
	bounds = sf::IntRect(column0, 6 * row1, m_PAWidth, m_PAHeight);
	m_texturePath.insert(std::make_pair(card, bounds));

	///================================== ACTION CARDS =================================//

	///* Set the texture postion for each action card */

	/* Action Cart - Lamp + Cart Fix id(2, 2, 0, 0, 0) */
	card = std::tuple(2, 2, 0, 0, 0);
	bounds = sf::IntRect(column0, row0, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Cart - Lamp + Pickaxe Fix id(2, 0, 2, 0, 0) */
	card = std::tuple(2, 0, 2, 0, 0);
	bounds = sf::IntRect(column1, row0, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Cart - Pickaxe + Cart Fix id(0, 2, 2, 0, 0) */
	card = std::tuple(0, 2, 2, 0, 0);
	bounds = sf::IntRect(2 * column1, row0, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Cart - Rockfall id(0, 0, 0, 1, 0) */
	card = std::tuple(0, 0, 0, 1, 0);
	bounds = sf::IntRect(3 * column1, row0, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Cart - Pickaxe broke id(0, 0, 1, 0, 0) */
	card = std::tuple(0, 0, 1, 0, 0);
	bounds = sf::IntRect(5 * column1, row0, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Cart - Pickaxe fix id(0, 0, 2, 0, 0) */
	card = std::tuple(0, 0, 2, 0, 0);
	bounds = sf::IntRect(8 * column1, row0, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Card - Broken Cart id (0, 1, 0, 0, 0) */
	card = std::tuple(0, 1, 0, 0, 0);
	bounds = sf::IntRect(column0, row1, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Card - Fixed Cart id (0, 2, 0, 0, 0) */
	card = std::tuple(0, 2, 0, 0, 0);
	bounds = sf::IntRect(3 * column1, row1, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Card - Broken Lamp id (1, 0, 0, 0, 0) */
	card = std::tuple(1, 0, 0, 0, 0);
	bounds = sf::IntRect(5 * column1, row1, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Card - Fixed Lamp id (2, 0, 0, 0, 0) */
	card = std::tuple(2, 0, 0, 0, 0);
	bounds = sf::IntRect(8 * column1, row1, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	/* Action Card - Card Reaveal id (0, 0, 0, 0, 1) */
	card = std::tuple(0, 0, 0, 0, 1);
	bounds = sf::IntRect(6 * column1, 6 * row1, m_PAWidth, m_PAHeight);
	m_textureAction.insert(std::make_pair(card, bounds));

	///================================== ICONS =================================//
	int id;
	const int IconWidth = 50;
	const int iconHeight = 50;

	/*Icon - Lamp id 1*/
	id = 1;
	bounds = sf::IntRect(column1, row1 + 3, IconWidth, iconHeight);
	m_textureIcons.insert(std::make_pair(id, bounds));
	
	/*Icon - Cart id 2*/
	id = 2;
	bounds = sf::IntRect(column1 + IconWidth, row1 + 3, IconWidth, iconHeight);
	m_textureIcons.insert(std::make_pair(id, bounds));
	
	/*Icon - Pick id 3*/
	id = 3;
	bounds = sf::IntRect(column1 + 2 * IconWidth, row1 + 3, IconWidth, iconHeight);
	m_textureIcons.insert(std::make_pair(id, bounds));

	/*Icon - GoldNugget id 4*/
	id = 4;
	bounds = sf::IntRect(column1 + 3 * IconWidth, row1 + 3, IconWidth, iconHeight);
	m_textureIcons.insert(std::make_pair(id, bounds));

	/*Icon - GoldNugget id 4*/
	id = 5;
	bounds = sf::IntRect(column1 + 4 * IconWidth, row1 + 3, IconWidth, iconHeight);
	m_textureIcons.insert(std::make_pair(id, bounds));
}

void Frontend::resetTriggered()
{
	for (int i = 0; i < sf::Keyboard::KeyCount; ++i)
		key_triggered[i] = false;
}

bool Frontend::isKeyTrigger(sf::Keyboard::Key key)
{
	if (sf::Keyboard::isKeyPressed(key) && !key_triggered[key])
	{
		return (key_triggered[sf::Keyboard::Right] = true);
	}
}

sf::IntRect Frontend::PathCardTextureSelecter(std::tuple<int, int, int, int, int> coord)
{
	sf::IntRect recFind = m_texturePath[coord];
	return recFind;
}

std::vector<sf::RectangleShape> Frontend::getInHandCards() const
{
	return m_cardsInHand;
}

std::vector<sf::RectangleShape> Frontend::getonBoardCards() const
{
	return m_cardsOnBoard;
}

std::vector<sf::Text> Frontend::getPlayers() const
{
	return m_players;
}

sf::RectangleShape & Frontend::getInHandCard(int pos)
{
	return m_cardsInHand.at(pos);
}

sf::RectangleShape & Frontend::getBoardCard(int pos)
{
	return m_cardsOnBoard.at(pos);
}

sf::Text & Frontend::getPlayer(int pos)
{
	return m_players.at(pos);
}

void Frontend::drawBoard(sf::RenderWindow& window, Board& board,const int pos)
{
	/* Current position in board */
	int vectorPosCounter = 0;

	/* We itterate vector like on a matrix */
	for (int i = 0; i < boardRows; ++i)
	{
		for (int j = 0; j < boardColumns; ++j)
		{
			int posY = i * renederCardHeight;
			int posX = j * renederCardWidth;

			PathCard currentCard;
			currentCard = board.getCardFromBoard(vectorPosCounter);

			/* Setup the size */
			m_cardsOnBoard.at(vectorPosCounter).setSize(sf::Vector2f(renederCardWidth, renederCardHeight));
			/* Setup position */
			m_cardsOnBoard.at(vectorPosCounter).setPosition(posX, posY);

			/* If the graphic rectangle at the position i is not an default, we will draw the card*/
			if (currentCard.getType() == Card::CardType::Path)
			{
				m_cardsOnBoard.at(vectorPosCounter).setFillColor(sf::Color::White);
				m_cardsOnBoard.at(vectorPosCounter).setTexture(&m_textures);

				/* Setup the texture */
				std::tuple <int, int, int, int, int> idCard = currentCard.getID();
				m_cardsOnBoard.at(vectorPosCounter).setTextureRect(m_texturePath.at(idCard));
			}
			
			/* If the current card is an empty space (a None type) */
			else if (currentCard.getType() == Card::CardType::None)
			{
				m_cardsOnBoard.at(vectorPosCounter).setFillColor(sf::Color::Transparent);
				m_cardsOnBoard.at(vectorPosCounter).setOutlineThickness(1);
			}

			/* If the current card is the start card */
			else if (currentCard.getType() == Card::CardType::Start)
			{
				/* Setup the graphics */
				m_cardsOnBoard.at(vectorPosCounter).setTexture(&m_textures);
				m_cardsOnBoard.at(vectorPosCounter).setTextureRect(m_texturePath.at(startCard));
			}
			
			/* If the current card is the finnish card */
			else if (currentCard.getType() == Card::CardType::Finish)
			{
				if (board.getCardFromBoard(vectorPosCounter + 1).getType() == Card::CardType::Path || pos == vectorPosCounter)
				{
					if (board.getCardFromBoard(vectorPosCounter + 1).getWestValue() == W::True || pos == vectorPosCounter)
					{
						/* Setup the graphics */
						m_cardsOnBoard.at(vectorPosCounter).setTexture(&m_textures);
						m_cardsOnBoard.at(vectorPosCounter).setTextureRect(m_texturePath.at(std::tuple(0, 1, 0, 1, 1)));
					}
				}
				else 
				{
					/* Setup the graphics */
					m_cardsOnBoard.at(vectorPosCounter).setTexture(&m_textures);
					m_cardsOnBoard.at(vectorPosCounter).setTextureRect(m_texturePath.at(finishCardHide));
				}
			}

			/* If the current card is the gold card */
			else if (currentCard.getType() == Card::CardType::FinishGold)
			{
				if (board.getCardFromBoard(vectorPosCounter + 1).getType() == Card::CardType::Path || pos == vectorPosCounter)
				{
					if (board.getCardFromBoard(vectorPosCounter + 1).getWestValue() == W::True || pos == vectorPosCounter)
					{
						/* Setup the graphics */
						m_cardsOnBoard.at(vectorPosCounter).setTexture(&m_textures);
						m_cardsOnBoard.at(vectorPosCounter).setTextureRect(m_texturePath.at(finishGoldCardUnhide));
					}
				}
				else
				{
					/* Setup the graphics */
					m_cardsOnBoard.at(vectorPosCounter).setTexture(&m_textures);
					m_cardsOnBoard.at(vectorPosCounter).setTextureRect(m_texturePath.at(finishCardHide));
				}
			}
			
			window.draw(m_cardsOnBoard.at(vectorPosCounter));
			++vectorPosCounter;
		}
	}

}

void Frontend::drawInCurrentPlayer(sf::RenderWindow & window, Player& player,const int& selectedCard )
{
	const int middleBoardXPostion = boardColumns * renederCardWidth / 2;

	std::vector<std::variant<PathCard, ActionCard>> inHandCards = player.getHandsCards();
	m_cardsInHand.resize(inHandCards.size());

	///* Draw name of a the current player */
	sf::Text title;
	///* =================================== SETUP TITLE ================================ ///
	
	std::string titleComposition = std::to_string(player.getGold()) + "x   "  + player.getName() + " - " + player.getTypeAsString();
	const int posNameX = middleBoardXPostion - titleComposition.length();
	const int posNameY = 5 * renederCardHeight + 5 /*px*/;
	{

		/* Set the postion on screen*/
		title.setPosition(sf::Vector2f(posNameX, posNameY));

		/* select the font */
		title.setFont(m_fontTitle);

		/* Set the player name */
		title.setString(titleComposition);

		/* set the color */
		title.setFillColor(sf::Color::Black);
	}

	///* =================================== SETUP GOLD ================================ ///
	sf::RectangleShape goldNugget;
	{		
		/* Setup the size */
		goldNugget.setSize(sf::Vector2f(30, 30));
		goldNugget.setPosition(posNameX + 50, posNameY + 5/* adjust the position with 5px*/);

		/* Setup the texture */
		goldNugget.setTexture(&m_textures);
		/* Action Card - Broken Lamp id 1 */
		goldNugget.setTextureRect(m_textureIcons.at(4));
		window.draw(goldNugget);
	}


	window.draw(goldNugget);
	window.draw(title);

	///* Draw each card from hand */
	{
		/*A gap between cards of ten pixels*/
		const int gap = 10;
		/*Distance where to start to draw cards from hand*/
		const int posCardY = 5 * renederCardHeight + renederCardHeight / 2;
		const int centerX = middleBoardXPostion - renederCardWidth * inHandCards.size() / 2;
						
		for (int i = 0; i < inHandCards.size(); ++i)
		{
			m_cardsInHand.at(i).setOutlineColor(sf::Color::Black);
			if (std::holds_alternative<PathCard>(inHandCards.at(i)))
			{
				PathCard pathCard = std::get<PathCard>(inHandCards.at(i));

				/* Setup the size */
				m_cardsInHand.at(i).setSize(sf::Vector2f(renederCardWidth, renederCardHeight));
				/* Setup position */
				m_cardsInHand.at(i).setPosition(i * renederCardWidth + (i * gap) + centerX, posCardY);

				/* Setup the texture */
				std::tuple <int, int, int, int, int> idCard = pathCard.getID();
				m_cardsInHand.at(i).setTexture(&m_textures);
				m_cardsInHand.at(i).setTextureRect(m_texturePath.at(idCard));

				m_cardsInHand.at(i).setOutlineThickness(0);
			}
			else /*if std::holds_alternative<ActionCard>(inHandCards.at(i)) */
			{
				ActionCard actionCard = std::get<ActionCard>(inHandCards.at(i));

				/* Setup the size */
				m_cardsInHand.at(i).setSize(sf::Vector2f(renederCardWidth, renederCardHeight));
				/* Setup position */
				m_cardsInHand.at(i).setPosition(i * renederCardWidth + (i * gap) + centerX, posCardY);

				/* Setup the texture */
				std::tuple <int, int, int, int, int> idCard = actionCard.getID();
				m_cardsInHand.at(i).setTexture(&m_textures);
				m_cardsInHand.at(i).setTextureRect(m_textureAction.at(idCard));

				m_cardsInHand.at(i).setOutlineThickness(0);
			}
			
			/* If the current card is the selected card */
			if (i == selectedCard)
			{
				m_cardsInHand.at(i).setOutlineThickness(2);
			}

			window.draw(m_cardsInHand.at(i));
		}
	}
}

void Frontend::drawThePlayers(sf::RenderWindow & window, std::vector<Player> & players)
{
	m_players.resize(players.size());

	const int posX = 9 * renederCardWidth + 10;
	
	
	///* Draw name of a the current player */
	sf::Text title;
	///* =============== SETUP TITLE ============= ///
		{
		std::string titleComposition = "Players:";

		/* Set the postion on screen*/
		title.setPosition(sf::Vector2f(posX, 5));

		/* select the font */
		title.setFont(m_fontTitle);

		/* Set the player name */
		title.setString(titleComposition);


		/* set the color */
		title.setFillColor(sf::Color::Black);
		}

	window.draw(title);



	for (int i = 0; i < players.size(); ++i)
	{
		const int posYAttr = (i + 1) * 50;
		//Setup text
		m_players.at(i).setString(players.at(i).getName());
		m_players.at(i).setPosition(posX, posYAttr);
		m_players.at(i).setFont(m_fontTitle);
		title.setFillColor(sf::Color::Black);

		//Set up attributes
		if (players.at(i).m_actionLamp)
		{
			const int posXAttr = 300 + posX;
			sf::RectangleShape lampAttr;

			/* Setup the size */
			lampAttr.setSize(sf::Vector2f(30, 30));
			lampAttr.setPosition(posXAttr, posYAttr - 2);

			/* Setup the texture */
			lampAttr.setTexture(&m_textures);
			/* Action Card - Broken Lamp id 1 */
			lampAttr.setTextureRect(m_textureIcons.at(1));
			window.draw(lampAttr);
		}


		if (players.at(i).m_actionCart)
		{
			const int posXAttr = 380 + posX;
			sf::RectangleShape cartAttr;

			/* Setup the size */
			cartAttr.setSize(sf::Vector2f(30, 30));
			cartAttr.setPosition(posXAttr, posYAttr - 2);

			/* Setup the texture */
			cartAttr.setTexture(&m_textures);
			/* Action Card - Broken Cart id 3 */
			cartAttr.setTextureRect(m_textureIcons.at(2));
			window.draw(cartAttr);
		}

		if (players.at(i).m_actionPickAxe)
		{
			const int posXAttr = 340 + posX;
			sf::RectangleShape pickaxeAttr;

			/* Setup the size */
			pickaxeAttr.setSize(sf::Vector2f(30, 30));
			pickaxeAttr.setPosition(posXAttr, posYAttr - 2);

			/* Setup the texture */
			pickaxeAttr.setTexture(&m_textures);
			/* Action Card - Broken Pickaxe id 2 */
			pickaxeAttr.setTextureRect(m_textureIcons.at(3));
			window.draw(pickaxeAttr);
		}

		window.draw(m_players.at(i));
	}
}

void Frontend::drawBackground(sf::RenderWindow& window)
{
	window.draw(m_background);
}

void Frontend::drawFinnishWindow(sf::RenderWindow & window, const std::string & winers)
{
	sf::RectangleShape miniWindow;
	///* ================================= SETUP MINI WINDOW ================================= ///
	const int windowWidth = 300;
	const int windowHeight = 150;

	miniWindow.setSize(sf::Vector2f(windowWidth, windowHeight));
	miniWindow.setPosition(window.getSize().x / 2 - windowWidth, window.getSize().y / 2 - windowHeight);
	miniWindow.setFillColor(sf::Color::Black);
	
	///* Draw the final window*/
	sf::Text title;

	///* ===================================== SETUP TITLE ================================== ///
	
	/* Set the postion on screen*/
	title.setPosition(sf::Vector2f(miniWindow.getPosition().x + 50, miniWindow.getPosition().y + 50));

	/* select the font */
	title.setFont(m_fontTitle);

	/* Set the player name */
	title.setString(winers);

	/* set the color */
	title.setFillColor(sf::Color::White);

	window.draw(miniWindow);
	window.draw(title);
}
