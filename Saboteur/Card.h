#pragma once
#include <iostream>


class Card
{

public:
	enum class CardType
	{
		Path,
		Action,
		Gold,
		Start,
		Finish,
		FinishGold,
		None
	};

public:
	/* Default constructor */
	Card() = default;
	
	/* Parametric constructor*/
	Card(CardType);

	///* Setters
	void setType(CardType cardType);
	///* Getters
	CardType getType() const;

protected:
	CardType m_cardType;
};

