#pragma once
#include "Card.h"
#include "PathCard.h"
#include "ActionCard.h"

#include <variant> 
#include <optional>
#include <array>
#include <vector>


class Board
{
public:
	/*Default constructor */
	Board();
	
	///*Getters
	void getCardFromBoardAndRemove(const int &pos);
	PathCard& getCardFromBoard(const int &pos);
	size_t getkLengthBoard();
	size_t getkWidthBoard();
	size_t getBoardSize();
	bool getCompletePath();
	
	///*Setters
	void setFinishCards();
	//Implemented on Player class
	void setCardOnBoard(std::variant<PathCard, ActionCard> &&card, const int &posInBoard);
	void setIfIsCompletePath(const bool &path);
	
	bool isBetweenFinishCard(const int &pos);

private:
	///*Size of the board
	static const size_t kWidthBoard = 5;
	static const size_t kLengthBoard = 9;
	static const size_t kSize = kLengthBoard * kWidthBoard;

	//array of the board
	std::array<PathCard, kSize> m_board;

	bool m_isCompletePath = false;
};