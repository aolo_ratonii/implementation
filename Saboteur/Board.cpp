#include "Board.h"
#include <iterator>
#include<iostream>
#include <iterator>
#include <variant>
#include <vector>
#include <random>
// name convention 00000 -> NONE card

const PathCard m_invalidCard(PathCard::North::False, PathCard::South::False, PathCard::West::False, PathCard::East::False, PathCard::Connected::False, PathCard::CardType::None);

Board::Board()
{
	for (int i = 0; i < m_board.size(); i++)
	{
		m_board[i].setType(Card::CardType::None);
		m_board[i].setNorthValue(PathCard::North::False);
		m_board[i].setSouthValue(PathCard::South::False);
		m_board[i].setWestValue(PathCard::West::False);
		m_board[i].setEastValue(PathCard::East::False);
		m_board[i].setConnectedValue(PathCard::Connected::False);
	}
	m_board[26].setType(Card::CardType::Start);
	setFinishCards();
}
void Board::setFinishCards()
{
	std::vector<Card> tempVector;
	Card card1;
	Card card2;
	Card card3;
	card1.setType(Card::CardType::Finish);
	card2.setType(Card::CardType::Finish);
	card3.setType(Card::CardType::FinishGold);
	tempVector.insert(tempVector.end(), { card2,card3,card1 });

	m_board[26].setType(Card::CardType::Start);

	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(tempVector.begin(), tempVector.end(), g);

	m_board[0].setType(tempVector.at(2).getType());
	tempVector.pop_back();
	m_board[18].setType(tempVector.at(1).getType());
	tempVector.pop_back();
	m_board[36].setType(tempVector.at(0).getType());
	tempVector.pop_back();
}
void Board::setCardOnBoard(std::variant<PathCard, ActionCard> &&card, int const &posInBoard)
{

	if (std::holds_alternative<PathCard>(card))
	{
		m_board.at(posInBoard) = std::move(std::get<PathCard>(card));
	}
	else
		m_board.at(posInBoard).setType(Card::CardType::None);
}
void Board::getCardFromBoardAndRemove(const int &pos)
{
	Card c = m_board.at(pos);
	m_board.at(pos).setType(Card::CardType::None);//on the removed card the place becomes empty and initialize with None type card
}
PathCard& Board::getCardFromBoard(const int & pos)
{
	if( pos >= 0 )
		return m_board.at(pos);
	
	PathCard kInvalid;
	return kInvalid;
}

size_t Board::getkLengthBoard()
{
	return kLengthBoard;
}

size_t Board::getkWidthBoard()
{
	return kWidthBoard;
}

size_t Board::getBoardSize()
{
	return (kLengthBoard * kWidthBoard);
}

bool Board::isBetweenFinishCard(const int & pos)
{
	if (pos == 9 || pos == 27)
		return true;
	return false;
}

void Board::setIfIsCompletePath(const bool & path)
{
	m_isCompletePath = path;
}

bool Board::getCompletePath()
{
	return m_isCompletePath;
}

