#pragma once

#include <tuple>
#include <cstdint>

#include "Card.h"


class ActionCard :
	public Card
{

public:

	enum class Lamp : uint8_t
	{
		None,
		Broken,
		Fixed
	};

	enum class Cart : uint8_t
	{
		None,
		Broken,
		Fixed
	};

	enum class Pickaxe : uint8_t
	{
		None,
		Broken,
		Fixed
	};

	enum class RockFall : uint8_t
	{
		None,
		True
	};

	enum class CardRevealed : uint8_t
	{
		None,
		True
	};


public:

	ActionCard(Lamp lamp = Lamp::None, Cart cart = Cart::None, Pickaxe pickAxe = Pickaxe::None, RockFall rockFall = RockFall::None, CardRevealed cardRevevaled = CardRevealed::None);
	friend std::ostream& operator << (std::ostream& os, const ActionCard& actionCard);
	///*Getters
	Lamp getLamp();
	Cart getCart();
	Pickaxe getPickAxe();
	RockFall getRockFall();
	CardRevealed getCardRevealed();

	/*
	This getters return a tuple with a specific action as follow : Lamp, Cart, Pickaxe, RockFall, CardRevealed where:
	0 - None
	1 - Broken (Lamp, Cart, Pickaxe) / True (RockFall, CardRevealed)
	2 - Fixed
	*/
	std::tuple<int, int, int, int, int> getID();


private:

	Lamp m_lamp : 2;
	Cart m_cart : 2;
	Pickaxe m_pickaxe : 2;
	RockFall m_rockFall : 1;
	CardRevealed m_cardRevealed : 1;
};

