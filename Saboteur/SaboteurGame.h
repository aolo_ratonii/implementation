#pragma once
#include "Player.h"
#include "Board.h"
#include "UnusedCards.h"
//#include "Logging/Logging.h"
#include "Frontend.h"

#include<iostream>
#include <iterator>


class SaboteurGame
{
public:
	void Run(int nrPlayers,const std::vector<std::string>& names, const std::vector<int>& ages);
	
	///* Setter 
	void setPlayersTypes(std::vector<Player>& players, std::vector<Player::PlayerType>& playerTypes);
	void setNumberOfPlayers(unsigned int numberOfPlayers);

	///* Getters
	unsigned int getNumberOfPlayers();
	int getPlayerPosition(std::vector<Player> &players, const Player &player);
	
	///*=============================== Auxiliary functions for "run" ========================================*/
	void loadTheTypes(std::vector<Player::PlayerType>& playerTypes);
	void dealTheCards(std::vector<Player>& players, UnusedCards& unusedCards);
	void dealTheGold(std::vector<Player>&, Player, GoldNuggets & goldNuggets);
	void typesOfPlayers(std::vector<Player>players, int &diggers, int &saboteurs);
	
	/*
	This function will take us to the next player
	first param. the postion of the curent player
	*/
	void nextPlayer(int& currentPlayerPosition);

private:
	unsigned int m_numberOfPlayers;
};
