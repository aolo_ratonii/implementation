#include "Card.h"
Card::Card(CardType cardType) :
	m_cardType(cardType)
{
	// Empty
}

void Card::setType(Card::CardType cardType)
{
	m_cardType = cardType;
}

Card::CardType Card::getType() const
{
	return m_cardType;
}
