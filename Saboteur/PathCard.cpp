#include "PathCard.h"
#include <algorithm>  

PathCard::PathCard() :
	Card(CardType::Path)
{
	//Empty
}

PathCard::PathCard(North north, South south, West west, East east, Connected connected, Card::CardType type) :
	Card(type),
	m_north(north),
	m_south(south),
	m_west(west),
	m_east(east),
	m_connected(connected)
{
	// Empty
}

PathCard::North PathCard::getNorthValue()
{
	return m_north;
}

void PathCard::setNorthValue(North north)
{
	m_north = north;
}

PathCard::South PathCard::getSouthValue()
{
	return m_south;
}

void PathCard::setSouthValue(South south)
{
	m_south = south;
}

PathCard::East PathCard::getEastValue()
{
	return m_east;
}

void PathCard::setEastValue(East east)
{
	m_east = east;
}

PathCard::West PathCard::getWestValue()
{
	return m_west;
}

void PathCard::setWestValue(West west)
{
	m_west = west;
}

PathCard::Connected PathCard::getConnectedValue()
{
	return m_connected;
}

void PathCard::setConnectedValue(Connected connected)
{
	m_connected = connected;
}

std::tuple<int, int, int, int, int> PathCard::getID()
{
	std::tuple <int, int, int, int, int> idCard = std::tuple(
		static_cast<int>(m_north),
		static_cast<int>(m_south),
		static_cast<int>(m_west),
		static_cast<int>(m_east),
		static_cast<int>(m_connected)
	);
	return idCard;
}

std::ostream & operator<<(std::ostream & os, const PathCard & pathCard)
{
	return os << static_cast<int>(pathCard.m_north) << static_cast<int> (pathCard.m_south) << static_cast <int> (pathCard.m_west) << static_cast <int> (pathCard.m_east) << static_cast <int> (pathCard.m_connected);
	// TODO: insert return statement here
}
