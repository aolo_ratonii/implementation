#include "ActionCard.h"

ActionCard::ActionCard(Lamp lamp, Cart cart, Pickaxe pickAxe, RockFall rockFall, CardRevealed cardRevevaled) :
	Card(CardType::Action),
	m_lamp(lamp),
	m_cart(cart),
	m_pickaxe(pickAxe),
	m_rockFall(rockFall),
	m_cardRevealed(cardRevevaled)
{
}

ActionCard::Lamp ActionCard::getLamp()
{
	return m_lamp;
}

ActionCard::Cart ActionCard::getCart()
{
	return m_cart;
}

ActionCard::Pickaxe ActionCard::getPickAxe()
{
	return m_pickaxe;
}

ActionCard::RockFall ActionCard::getRockFall()
{
	return m_rockFall;
}

ActionCard::CardRevealed ActionCard::getCardRevealed()
{
	return m_cardRevealed;
}

std::tuple<int, int, int, int, int> ActionCard::getID()
{
	std::tuple <int, int, int, int, int> idCard = std::tuple(
		static_cast<int>(m_lamp),
		static_cast<int>(m_cart),
		static_cast<int>(m_pickaxe),
		static_cast<int>(m_rockFall),
		static_cast<int>(m_cardRevealed)
	);
	return idCard;
}

std::ostream & operator<<(std::ostream & os, const ActionCard & actionCard)
{
	return os << static_cast<int> (actionCard.m_lamp) << static_cast<int>  (actionCard.m_cart) << static_cast<int> (actionCard.m_pickaxe) << static_cast<int>  (actionCard.m_rockFall) << static_cast<int>  (actionCard.m_cardRevealed);
}
