#include "SaboteurGame.h"
#include "Player.h"

void SaboteurGame::loadTheTypes(std::vector<Player::PlayerType>& playerTypes)
{
	//There will be a saboteur
	if (m_numberOfPlayers < 5)
	{
		playerTypes[playerTypes.size() - 1] = Player::PlayerType::Saboteur;
	}
	//There will be two saboteurs
	if (m_numberOfPlayers > 4 && m_numberOfPlayers < 7)
	{
		playerTypes[playerTypes.size() - 1] = Player::PlayerType::Saboteur;
		playerTypes[playerTypes.size() - 2] = Player::PlayerType::Saboteur;
	}
	//There will be three saboteurs
	else if (m_numberOfPlayers > 6 && m_numberOfPlayers < 9)
	{
		playerTypes[playerTypes.size() - 1] = Player::PlayerType::Saboteur;
		playerTypes[playerTypes.size() - 2] = Player::PlayerType::Saboteur;
		playerTypes[playerTypes.size() - 3] = Player::PlayerType::Saboteur;
	}
	else if (m_numberOfPlayers == 10)
	{
		playerTypes[playerTypes.size() - 1] = Player::PlayerType::Saboteur;
		playerTypes[playerTypes.size() - 2] = Player::PlayerType::Saboteur;
		playerTypes[playerTypes.size() - 3] = Player::PlayerType::Saboteur;
		playerTypes[playerTypes.size() - 4] = Player::PlayerType::Saboteur;
	}
}
void SaboteurGame::setPlayersTypes(std::vector<Player>& players, std::vector<Player::PlayerType>& playerTypes)
{
	for (int i = 0; i < players.size(); ++i)
	{
		players[i].setType(playerTypes[i]);
	}
}

void SaboteurGame::dealTheCards(std::vector<Player>& players, UnusedCards & unusedCards)
{
	int numberOfCards;
	//Each player take six cards
	if (m_numberOfPlayers > 2 && m_numberOfPlayers < 6)
		numberOfCards = 6;
	//Each player take 5 cards
	else if (m_numberOfPlayers > 5 && m_numberOfPlayers < 8)
		numberOfCards = 5;
	//Each player take 4 cards
	else if (m_numberOfPlayers > 7 && m_numberOfPlayers < 11)
		numberOfCards = 4;

	for (int i = 0; i < numberOfCards; ++i)
	{
		for (int j = 0; j < players.size(); ++j)
		{

			players[j].PickUnusedCards(unusedCards);
		}
	}
}

void SaboteurGame::dealTheGold(std::vector<Player>& players, Player winner, GoldNuggets & goldNuggets/*This is an vector of NuggetCard*/)
{
	goldNuggets.sortDescCards();
	int pos = getPlayerPosition(players, winner);
	int noOfDiggers = 0;
	int noOfSaboteurs = 0;
	typesOfPlayers(players, noOfDiggers, noOfSaboteurs);

	if (winner.getType() == Player::PlayerType::Digger)
	{
		for (int i = 0; i < players.size(); i++)
		{
			if (players.at(i).getType() == Player::PlayerType::Digger)
			{
				if (pos == 0)
					pos = players.size() - 1;
				else
				{
					players.at(pos).amountOfGold += static_cast<int32_t>(goldNuggets.takeCard().getQuantity());
					pos--;//Go on the left direction
				}
			}
		}
	}

	else if (winner.getType() == Player::PlayerType::Saboteur)
	{
		for (int i = 0; i < noOfDiggers; i++)
		{
			if (players.at(i).getType() == Player::PlayerType::Saboteur)
				if (pos == 0)
					pos = noOfDiggers;
				else
				{
					players.at(pos).amountOfGold += static_cast<int32_t>(goldNuggets.takeCard().getQuantity());
					pos--;
				}
		}
	}
}

void SaboteurGame::typesOfPlayers(std::vector<Player>players, int &diggers, int &saboteurs)
{
	for (int i = 0; i < players.size(); i++)
	{
		if (players.at(i).getType() == Player::PlayerType::Digger)
			diggers++;
		else
			saboteurs++;

	}
}

void SaboteurGame::nextPlayer(int& currentPlayerPosition)
{
	/* We iterate in a circle like form, so if we got the last player after him we go to the first player again */
	if (currentPlayerPosition < m_numberOfPlayers - 1)
		currentPlayerPosition++;
	else
		currentPlayerPosition = 0;
}



void SaboteurGame::Run(int nrPlayers, const std::vector<std::string>& names, const std::vector<int>& ages)
{
	///*Debuging puropses*/
	//Logger log(std::cout, Logger::Level::Info);

	/* Record elapsed time of next player appear*/
	float m_SecondsSinceNextPlayer = 0;

	//TODO graphic interface for this
	m_numberOfPlayers = NULL;
	//while (m_numberOfPlayers == NULL)
	//{
	//	unsigned int numberOfPlayers;
	//	std::cout << "\nHow many players will be?: ";
	//	std::cin >> numberOfPlayers;
	//	this->setNumberOfPlayers(numberOfPlayers);
	//}
	this->setNumberOfPlayers(nrPlayers);

	std::vector<Player> players;
	players.resize(m_numberOfPlayers);

	std::cout << std::endl;

	/* Set every player */
	unsigned minimumAge = INT_MAX;
	for (int i = 0; i < players.size(); ++i)
	{
		/* Set the name for the current player */
		//std::string name;
		//std::cout << "Player " << i + 1 << " name:";
		//std::cin >> name;
		players[i].setName(names[i]);

		/* Set the age for the current player*/
		//unsigned int age;
		//std::cout << "Player " << i + 1 << " age:";
		//std::cin >> age;
		players[i].setAge(ages[i]);

		/* Verify if the current player have the minimum age at this moment */
		if (ages[i] < minimumAge)
			minimumAge = ages[i];
	}

	const int numberOfGames = 3;
	unsigned startPosition = NULL;

	GoldNuggets goldNuggets;

	for (unsigned i = 0; i < numberOfGames; ++i)
	{
		///*Intialization frontend*/
		Frontend frontend;
		sf::RenderWindow window(sf::VideoMode(1366, 720, 32), "SFML Window"/*, sf::Style::Fullscreen*/);

		/*Set the refresh rate */
		window.setFramerateLimit(Frontend::FPS60);

		sf::View view(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(1700, 1080));
		sf::Clock m_Clock;

		///*Intialization backend*/
		Board board;
		UnusedCards unusedCards;

		/* Initialize a vector of player types */
		std::vector<Player::PlayerType> playerTypes;
		playerTypes.resize(m_numberOfPlayers + 1, Player::PlayerType::Digger);

		/* Sets the correct ratio between diggers and saboteurs */
		loadTheTypes(playerTypes);

		/* Shuffle the types */
		std::random_device rd;
		std::mt19937 g(rd());
		std::shuffle(playerTypes.begin(), playerTypes.end(), g);

		/* Each player will take a role */
		setPlayersTypes(players, playerTypes);

		/* Shuffle the unused cards*/
		unusedCards.ShuffleCards();

		for (Player& player : players)
		{
			if (player.getHandsCards().size() != 0)
			{
				for (int i = 0; player.getHandsCards().size(); ++i)
				{
					player.getHandsCards().pop_back();
				}
			}
		}

		/* Each player will take a number of cards in hand*/
		dealTheCards(players, unusedCards);

		/*A path is not created yet*/
		board.setIfIsCompletePath(false);


		/* The game starts with the youngest player */
		for (auto ittr = players.begin(); ittr != players.end(); ++ittr)
		{
			auto currentPos = std::distance(players.begin(), ittr);
			if (players.at(currentPos).getAge() == minimumAge)
			{
				startPosition = currentPos;
				break;
			}
		}

		int playerPos = startPosition;

		int lastSelectedHandCard = NULL;

		bool delayAtEnd = false;
		int lastPlayer = -1;

		int cardRevealed = NULL;
		while (window.isOpen())
		{
			sf::Time elapsedTime = m_Clock.restart();
			sf::Event event;

			///=================================== EVENT HANDLER =================================///
			while (window.pollEvent(event))
			{
				/*Close me the sfml window if the esc keyword is pressed*/
				if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
					window.close();

				///========================== DEBUGING ==========================///
				/*Look at each player when press right arrow, only debug puroposes*/
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
					SaboteurGame::nextPlayer(playerPos);
				///========================== DEBUGING ==========================///
			}


			///*  Hover effect */

			/*Retain where the left click was pressed*/
			//sf::Vector2i mousePos = sf::Mouse::getPosition(window);
			//for (int i = 0; i < frontend.getonBoardCards().size(); ++i)
			//	{
			//		if (frontend.getBoardCard(i).getGlobalBounds().contains(sf::Vector2f(mousePos)))
			//		{
			//			if (board.getCardFromBoard(i).getType() == Card::CardType::None )
			//			{
			//				if (lastSelectedHandCard < players.at(playerPos).getHandsCards().size())
			//				{
			//					std::tuple id = std::get<PathCard>(players.at(playerPos).getHandsCards().at(lastSelectedHandCard)).getID();
			//	
			//					if (std::holds_alternative<PathCard>((players.at(playerPos).getHandsCards().at(lastSelectedHandCard))))
			//					{
			//						if (id != std::make_tuple(0, 0, 0, 0, 0))
			//						{
			//							frontend.getBoardCard(i).setTexture(&frontend.m_textures);
			//							frontend.getBoardCard(i).setTextureRect(frontend.m_texturePath.at(id));
			//						}
			//					}
			//				}
			//			}
			//		}
			//	
			//		else
			//		{
			//			frontend.getBoardCard(i).setTexture(&frontend.m_textures);
			//			frontend.getBoardCard(i).setTextureRect(frontend.m_textureIcons.at(5));
			//		}
			//	}

			if (frontend.m_gameState == Frontend::GameState::Normal)
			{
				/*Left click*/
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					/*Retain where the left click was pressed*/
					sf::Vector2i mousePos = sf::Mouse::getPosition(window);

					///* TODO Make this for in 3 separate functions 

					/*Check if the hand was pressed*/
					for (int i = 0; i < frontend.getInHandCards().size(); ++i)
					{
						/* Verify if I clicked on a card from in hand card and what card was pressd*/
						if (frontend.getInHandCard(i).getGlobalBounds().contains(sf::Vector2f(mousePos)))
						{
							lastSelectedHandCard = i;
							///========================== DEBUGING ==========================///
							std::cout << "\nLast card selected :" << i;
							///========================== DEBUGING ==========================///
						}
					}

					/*Check if the board was pressed*/
					for (int i = 0; i < frontend.getonBoardCards().size(); ++i)
					{

						bool cuurentPlayerNotBroke = !(players.at(playerPos).m_actionCart || players.at(playerPos).m_actionLamp || players.at(playerPos).m_actionPickAxe);
						/*If we have an path card*/
						if (lastSelectedHandCard < players.at(playerPos).getHandsCards().size())
						{
							/*If a player can place a path card then the player will try to place it*/
							if (cuurentPlayerNotBroke)
							{
								/*The selected card must be path*/
								if (std::holds_alternative<PathCard>(players.at(playerPos).getHandsCards().at(lastSelectedHandCard)))
									if (frontend.getBoardCard(i).getGlobalBounds().contains(sf::Vector2f(mousePos)))
									{
										if (lastSelectedHandCard < players.at(playerPos).getHandsCards().size())
										{
											bool isNextPlayer = players.at(playerPos).PlacePathCard(board, lastSelectedHandCard, i);
											//players.at(playerPos).PlacePathCard(board, lastSelectedHandCard, i);

											/*If the card was placed successfully*/
											if (isNextPlayer)
											{
												/*Current player will take a card*/
												players.at(playerPos).PickUnusedCards(unusedCards);
												frontend.m_gameState = Frontend::GameState::NextPlayer;
												///========================== DEBUGING ==========================///
												std::cout << "Next player";
												///========================== DEBUGING ==========================///
											}

											/*The round/game ended cause' we reach the gold card*/
											if (board.getCompletePath() && lastPlayer == -1)
											{
												delayAtEnd = true;
												lastPlayer = playerPos;
											}

											///========================== DEBUGING ==========================///
											std::cout << "\nPlaced at postion " << i << std::endl;
											std::cout << "\nIs next player " << isNextPlayer << std::endl;
											///========================== DEBUGING ==========================///
										}
									}
							}

							else
							{
								///====================================== DEBUGING ==============================================///
								std::cout << std::endl << players.at(playerPos).getName() << "can't place cause' have a broke tool";
								///====================================== DEBUGING ==============================================///
							}

						}
						
						/* If we have an action card*/
						if (std::holds_alternative<ActionCard>(players.at(playerPos).getHandsCards().at(lastSelectedHandCard)))
						{
							ActionCard actionCard = std::get<ActionCard>(players.at(playerPos).getHandsCards().at(lastSelectedHandCard));
							///* The only action cards that can be place on board are rockfall and mapreveal */
												
							if (frontend.getBoardCard(i).getGlobalBounds().contains(sf::Vector2f(mousePos)))
							{
								/* If we selected rock fall */
								if (actionCard.getRockFall() == ActionCard::RockFall::True)
								{
									bool isCardDestroyed = players.at(playerPos).removeCardFromBoard(board, i);

									/* Take down the card from hand */
									if (isCardDestroyed)
									{
										players.at(playerPos).getHandsCards().erase(players.at(playerPos).getHandsCards().begin() + lastSelectedHandCard);
										players.at(playerPos).PickUnusedCards(unusedCards);
										frontend.m_gameState = Frontend::GameState::NextPlayer;
									}

									///====================================== DEBUGING ==============================================///
									std::cout << std::endl << "A card was removed from board at position " << i;
									///====================================== DEBUGING ==============================================///
								}

								/* If we selected map revealed*/
								else if (actionCard.getCardRevealed() == ActionCard::CardRevealed::True)
								{
									if (frontend.getBoardCard(i).getGlobalBounds().contains(sf::Vector2f(mousePos)))
									{
										/* Card at postion 0 - upper */
										int upperCard = 0, middleCard = 18, downCard = 36;
										if (i == upperCard || i == middleCard || i == downCard)
										{

											cardRevealed = i;

											///====================================== DEBUGING ==============================================///
											std::cout << std::endl << "A card was revealed at postion " << i;
											///====================================== DEBUGING ==============================================///
											players.at(playerPos).getHandsCards().erase(players.at(playerPos).getHandsCards().begin() + lastSelectedHandCard);
											players.at(playerPos).PickUnusedCards(unusedCards);
											frontend.m_gameState = Frontend::GameState::CardRevealed;
										}
									}
								}
							}

						}
					}

					/*Check if a player was pressed*/
					for (int i = 0; i < frontend.getPlayers().size(); ++i)
					{
						if (std::holds_alternative<ActionCard>(players.at(playerPos).getHandsCards().at(lastSelectedHandCard)))
							if (frontend.getPlayer(i).getGlobalBounds().contains(sf::Vector2f(mousePos)))
							{
								ActionCard currentCard = std::get<ActionCard>(players.at(playerPos).getHandsCards().at(lastSelectedHandCard));
								bool actionSucceeded = players.at(playerPos).PlaceActionCardAtPlayer(players.at(i), currentCard);

								if (actionSucceeded)
								{
									players.at(playerPos).getHandsCards().erase(players.at(playerPos).getHandsCards().begin() + lastSelectedHandCard);

									if (unusedCards.getUnusedCards().size() > 0)
									{
										players.at(playerPos).PickUnusedCards(unusedCards);
									}

									frontend.m_gameState = Frontend::GameState::NextPlayer;
								}

							}
					}
				}

				/*Rotate*/
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				{
					frontend.m_gameState = Frontend::GameState::RotateCard;
				}

				/*Discard*/
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
				{
					/*TODO move this to backend and call it in frontend*/
					players.at(playerPos).getHandsCards().erase(players.at(playerPos).getHandsCards().begin() + lastSelectedHandCard);
					if (unusedCards.getUnusedCards().size() > 0)
					{
						players.at(playerPos).PickUnusedCards(unusedCards);
					}
					frontend.m_gameState = Frontend::GameState::NextPlayer;
				}

			}

			if (frontend.m_gameState == Frontend::GameState::RotateCard)
			{
				m_SecondsSinceNextPlayer += elapsedTime.asSeconds();
				if (m_SecondsSinceNextPlayer > 0.2f)
				{
					///========================== DEBUGING ==========================///
					std::cout << "Cooldown ended." << std::endl;
					///========================== DEBUGING ==========================///

					/* Rotate */
					PathCard rotatedPath = players.at(playerPos).getPathCard(lastSelectedHandCard);
					Player::RotateCardToFit(rotatedPath);
					players.at(playerPos).setCard(rotatedPath, lastSelectedHandCard);
					std::cout << "\nRotate card";
					frontend.m_gameState = Frontend::GameState::Normal;

					m_SecondsSinceNextPlayer = 0;
				}


			}

			if (frontend.m_gameState == Frontend::GameState::NextPlayer)
			{
				m_SecondsSinceNextPlayer += elapsedTime.asSeconds();
				if (m_SecondsSinceNextPlayer > 2.0f)
				{
					///========================== DEBUGING ==========================///
					std::cout << "Cooldown ended. Player changed" << std::endl;
					///========================== DEBUGING ==========================///

					/* Next player */
					SaboteurGame::nextPlayer(playerPos);

					frontend.m_gameState = Frontend::GameState::Normal;

					lastSelectedHandCard = 0;
					m_SecondsSinceNextPlayer = 0;
				}
			}

			if (frontend.m_gameState == Frontend::GameState::EndRound)
			{
				/*The players will take gold*/
				dealTheGold(players, players.at(playerPos), goldNuggets);
				window.close();

				///========================== DEBUGING ==========================///
				std::cout << "A round eneded" << std::endl;
				///========================== DEBUGING ==========================///
			}
			
			///=================================== UPDATE SCREEN =================================//

			window.clear();
			//TODO that hover

			/* Draw background */
			frontend.drawBackground(window);

			/* Draw board on display as usual */
			if (frontend.m_gameState != Frontend::GameState::CardRevealed)
				frontend.drawBoard(window, board);

			/* Draw board on display with a card revealed*/
			if (frontend.m_gameState == Frontend::GameState::CardRevealed)
			{
				frontend.drawBoard(window, board, cardRevealed);
				m_SecondsSinceNextPlayer += elapsedTime.asSeconds();
				if (m_SecondsSinceNextPlayer > 5.0f)
				{
					frontend.m_gameState = Frontend::GameState::NextPlayer;
					m_SecondsSinceNextPlayer = 0;

					///====================================== DEBUGING ==============================================///
					std::cout << std::endl << "Delay ended, card " << cardRevealed << " was showed";
					///====================================== DEBUGING ==============================================///
				}
			}

			/* Draw current player */
			frontend.drawInCurrentPlayer(window, players.at(playerPos), lastSelectedHandCard);

			/* Draw all players */
			frontend.drawThePlayers(window, players);

			if (delayAtEnd == true)
			{
				m_SecondsSinceNextPlayer += elapsedTime.asSeconds();

				if (players.at(lastPlayer).getType() == Player::PlayerType::Digger)
					frontend.drawFinnishWindow(window, "Digger won");
				else
					frontend.drawFinnishWindow(window, "Saboteur won");

				if (m_SecondsSinceNextPlayer > 4.0f)
				{
					frontend.m_gameState = Frontend::GameState::EndRound;
					m_SecondsSinceNextPlayer = 0;
				}
			}

			window.display();

		}
	}
}
void SaboteurGame::setNumberOfPlayers(unsigned int numberOfPlayers)
{
	//Logger log(std::cout, Logger::Level::Info);

	if (numberOfPlayers < 3)
	{
		//log.log("Not enough players", Logger::Level::Info);
	}
	else if (numberOfPlayers > 10)
	{
		//log.log("Too many players", Logger::Level::Info);
	}
	else m_numberOfPlayers = numberOfPlayers;
}

unsigned int SaboteurGame::getNumberOfPlayers()
{
	return m_numberOfPlayers;
}

int SaboteurGame::getPlayerPosition(std::vector<Player> &players, const Player & player)
{
	for (int i = 0; i < m_numberOfPlayers; i++)
	{
		if (players.at(i).getName() == player.getName())
		{
			return i;
		}

	}
}