#pragma once
#include"Card.h"


class NuggetCard
{

public:

	/* All possibile gold cards*/
	enum class Quantity
	{
		One = 1,
		Two,
		Three
	};

	/*Default constructor*/
	NuggetCard() = default;
	/*paramteric constructor*/
	NuggetCard(Quantity amount);

	///*Setters 
	void setQuantity(Quantity amount);
	
	///*Getters
	Quantity getQuantity();

private:
	Quantity m_amount;
};

