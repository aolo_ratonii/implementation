#pragma once
#include "Card.h"
#include"PathCard.h"
#include"ActionCard.h"

#include <cstdint>
#include <tuple>


class PathCard :
	public Card
{
public:

	enum class North : uint8_t
	{
		False,
		True
	};

	enum class South : uint8_t
	{
		False,
		True
	};

	enum class West : uint8_t
	{
		False,
		True
	};

	enum class East : uint8_t
	{
		False,
		True
	};

	enum class Connected : uint8_t
	{
		False,
		True
	};

public:
	/*Default constructor*/
	PathCard();
	PathCard(North north, South south, West west, East east, Connected connected, Card::CardType type = Card::CardType::Path);
	friend std::ostream& operator << (std::ostream& os, const PathCard& pathCard);

	///* Getters
	North getNorthValue();
	South getSouthValue();
	West getWestValue();
	East getEastValue();
	Connected getConnectedValue();
	
	///*Setters
	void setNorthValue(North north);
	void setSouthValue(South south);
	void setWestValue(West west);
	void setEastValue(East east);
	void setConnectedValue(Connected connected);

	/*This getters return a tuple with coord. as follow : N, S, W, E, C, where C means conecteds*/
	std::tuple<int, int, int, int, int> getID();

private:
	North m_north : 1;
	South m_south : 1;
	West m_west : 1;
	East m_east : 1;
	Connected m_connected : 1;
};
